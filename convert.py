
class Stack:

	def __init__(self):
		self.items = []

	def isEmpty(self):
		return self.items == []

	def push(self, item):
		self.items.append(item)

	def pop(self):
		return self.items.pop()

	def peek(self):
		return self.items[len(self.items)-1]

	def size(self):
		return len(self.items)

	def printStack(self):

		stackString = ""

		while not self.isEmpty():
			stackString = stackString + str(self.pop())

		print(stackString)


class binaryConversion():

	def __init__(self, numToConvert):
		self.decimalNum = numToConvert
		self.stack = Stack()

	def convert(self):
		
		while self.decimalNum > 0:
			 remainder = self.decimalNum % 2
			 self.stack.push(remainder)
			 self.decimalNum = self.decimalNum // 2

	def printConversion(self):

		self.stack.printStack()


s = Stack()
s.push(1)
s.push(2)
s.push(3)

s.printStack()

b = binaryConversion(233)
b.convert()
b.printConversion()






